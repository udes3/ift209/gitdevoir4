.include "macros.s"
.global  main

main:
    adr     x0,  fmtLecture              // scanf(fmtLecture, chaine)
    adr     x1,  chaine                  //
    bl      scanf                        //
    adr     x19, chaine                  //

    adr     x0,  fmtEntree               // scanf(fmtEntree, code)
    adr     x1,  code                    //
    bl      scanf                        //
    ldr     x20, code                    //
cas0:
    cmp     x20, 0                       // if(code == 0){
    b.ne    cas1                         //
    mov     x0,  x19                     //    taille_chaine(chaine);
    bl      taille_chaine                //
    b       fin                          // }
cas1:
    cmp     x20, 1                       // if(code == 1){
    b.ne    cas2                         //
    mov     x0,  x19                     //     casses_substitutions(chaine);
    bl      casses_substitutions         //
    b       fin                          // }
cas2:
    cmp     x20, 2                       // if(code == 2){
    b.ne    cas3                         //
    mov     x0,  x19                     //     hexaDeci(chaine);
    bl      hexaDeci                     //
    b       fin                          // }
cas3:
    cmp     x20, 3                       // if(code == 3){
    b.ne    cas4                         //
    mov     x0, x19                      //     binDec(chaine);
    bl      binDec                       //
    b       fin                          // }
cas4:
    cmp     x20, 4                       // if(code == 4){
    b.ne    cas5                         //
    mov     x0, x19                      //     chiffrement(chaine);
    bl      chiffrement                  //
    b       fin                          // }
cas5:
    mov     x20, 0                       // if(code == 5){
    mov     x0, x19                      //
    mov     x1, x20                      //    permutation(chaine, 0);
    bl      permutation                  // }
fin:
    mov     x0, 0                        //
    bl      exit                         //

//=======================================//
//Opération 0 : Taille de la chaine      //
taille_chaine:
    SAVE                                 //
    mov     x19, x0                      // x19 = chaine;
    mov     x20, 0                       // x20 = nbElements = 0;
    mov     x28, 0                       // x28 = index;
boucle_taille:
                                         // while(caract != 0){
    ldrb    w21, [x19,x28]               //    caract = chaine[index];
    cmp     w21, 0                       //    conditions if ci-dessous
    b.eq    retour0                      //

    cmp     w21, 192                     //
    b.lo    debut0                       //

    cmp     w21, 224                     //
    b.lo    debut110                     //

    cmp     w21, 238                     //
    b.lo    debut1110                    //
                                         //    if(Début par 11110){
    add     x28, x28, 4                  //        index += 4;
    add     x20, x20, 1                  //        nbElements++;
    b       boucle_taille                //    }
debut0:
    add     x28, x28, 1                  //    if(Début par 0){
    add     x20, x20, 1                  //        index += 1;
    b       boucle_taille                //        nbElements++;
debut110:                                //    }
    add     x28, x28, 2                  //    if(Début par 110){
    add     x20, x20, 1                  //        index += 2;
    b       boucle_taille                //        nbElements++;
debut1110:                               //    }
    add     x28, x28, 3                  //    if(Début par 1110){
    add     x20, x20, 1                  //        index += 3;
    b       boucle_taille                //        nbElements++;
retour0:                                 //    }
                                         // }
    adr     x0, fmtSortie                // printf(fmtSortie, nbElements)
    mov     x1, x20                      //
    bl      printf                       //
    mov     x0, x20                      //
    RESTORE                              //
    ret                                  //

//=======================================//
//Opération 1 : Casses et substitutions  //
casses_substitutions:
    SAVE
    mov     x19, x0
    mov     x28, 0                       // i = 0
boucle_caractere:
    ldrb    w20, [x19,x28]               // while(symb != 0){
    cbz     w20, retour1                 //     symb = chaine[i]
    tbnz    x28, 0, pos_impair           //     if(i pair)
pos_pair:
    cmp     w20, 0x41                    //         if(symb > A && symb < Z){
    b.lo    remplacement                 //
    cmp     w20, 0x5A                    //
    b.hi    remplacement                 //              symb = symb en majuscule;
    eor     w20, w20, 0x20               //
    b       remplacement                 //         }
pos_impair:
    cmp     w20, 0x61                    //     else if(i impair){
    b.lo    remplacement                 //         if(symb > a && symb < z){
    cmp     w20, 0x7A                    //             symb = symb en minuscule;
    b.hi    remplacement                 //         }
    eor     w20, w20, 0x20               //     }
remplacement:
    bic     w21, w20, 0x20               //     copie = symb en majuscule
Aa4:
    cmp     w21, 0x41                    //     if(copie == A){
    b.ne    Ee3                          //         symb = 4;
    mov     w20, 0x34                    //     }
Ee3:
    cmp     w21, 0x45                    //     if(copie == E){
    b.ne    Ii1                          //         symb = 3;
    mov     w20, 0x33                    //     }
Ii1:
    cmp     w21, 0x49                    //     if(copie == I){
    b.ne    Oo0                          //         symb = 1;
    mov     w20, 0x31                    //     }
Oo0:
    cmp     w21, 0x4F                    //     if(copie == O){
    b.ne    fin_boucle_caractere         //         symb = 0;
    mov     w20, 0x30                    //     }
fin_boucle_caractere:
    strb    w20, [x19,x28]               //     chaine[i] = symb;
    add     x28, x28, 1                  //     i++;
    b       boucle_caractere             // }
retour1:
    adr     x0, fmtSortie2
    mov     x1, x19
    bl      printf
    mov     x0, 0
    RESTORE
    ret
//=======================================//
//Opération 2 : Hexadecimal vers decimal //
hexaDeci:
    SAVE
    mov     x19, x0
    mov     x28, 2                       // length = 2
// Trouve la longeur de la chaine
taille_chaine_op2:
    ldr     x20, [x19,x28]
    cbz     x20, fin_taille_chaine_op2
    add     x28, x28, 1                  // length += 1
    b       taille_chaine_op2
fin_taille_chaine_op2:
    mov     x21, 0                       // res = 0
    mov     x27, 1                       // exp = 1
    mov     x26, 16                      // const k = 16
boucle_conversion_hexa:
    sub     x28, x28, 1                  // length = length - 1
    cmp     x28, 1                       // while (length > 1){
    b.eq    retour2                      //
    ldrb    w20, [x19,x28]               //    val = string[x28]
    sub     x22, x20, 55                 //
    sub     x23, x20, 48                 //
    cmp     x20, 65                      //    if (val >= A){val = val - 55;}
    csel    x20, x22, x23, hs            //    else {val = val - 48}
fin_boucle_conversion_hexa:
    mul     x20, x20, x27                //    val = val * exp
    add     x21, x21, x20                //    res = res + val
    mul     x27, x27, x26                //    exp = exp * 16
    b       boucle_conversion_hexa       // }
retour2:
    adr     x0, fmtSortie
    mov     x1, x21
    bl      printf
    mov     x0, 0
    RESTORE
    ret
//=======================================//
//Opération 3 : Binaire vers decimal     //
binDec:
    SAVE
    mov     x19, x0
    mov     x28, 2                       // long = 2
// Trouve la longeur de la chaine
taille_chaine_op3:
    ldr     x20, [x19,x28]
    cbz     x20, fin_taille_chaine_op3
    add     x28, x28, 1                  // long += 1
    b       taille_chaine_op3
fin_taille_chaine_op3:
    mov     x21, 0                       // res = 0
    mov     x27, 1                       // exp = 1
    mov     x26, 2                       // const k = 2
boucle_conversion_bin:
    sub     x28, x28, 1                  // long = long - 1
    cmp     x28, 1                       // while (long != 1){
    b.eq    retour3                      //
    ldrb    w20, [x19,x28]               //    val = string [x28]
    sub     x20, x20, 48                 //    val = val - 48
    cmp     x28, 2                       //    if (x28 == 2){
    b.ne    fin_boucle_bin               //
    cmp     x20, 1                       //        if(val == 1){
    b.ne    fin_boucle_bin               //
    mul     x20, x20, x27                //          val = val * exp
    sub     x21, x21, x20                //          res = res - val; }
    b       retour3                      //    }
fin_boucle_bin:
    mul     x20, x20, x27                //    val = val * exp
    adds     x21, x21, x20               //    res = res + val
    mul     x27, x27, x26                //    exp = exp * 2
    b       boucle_conversion_bin        // }
retour3:
    adr     x0, fmtSortie3
    mov     x1, x21
    bl      printf
    mov     x0, 0
    RESTORE
    ret
//=======================================//
//Opération 4 : Chiffrement par decalage //
chiffrement:
    SAVE
    mov     x19, x0                      // x19 chaine de caracteres
boucle_transformation:
    mov     x28, 0                       // while(oldL != 0){
    mov     x21, 64                      //    j = 0
    ldrb    w20, [x19]                   //    oldL = chaine[i]
    cbz     w20, retour4                 //    newL = 0100 0000
    and     w23, w20, 7                  //
    lsl     w23, w23, 2                  //    droite = decalage(xxx1 11xx -> xxxx x111)
    and     w22, w20, 24                 //
    lsr     w22, w22, 3                  //    gauche = decalage(xxxx xx11 -> xxx1 1xxx)
    orr     w22, w23, w22                //
    orr     w21, w21, w22                //    newL = "010" + gauche + droite
decal_lettre:
    cmp     x28, 6                       //    while(j <= 6) {
    b.hi    fin_decal_lettre             //
    cmp     w21, 65                      //        if(newL < A){
    b.hs    suite_decal_lettre           //
    mov     w21, 90                      //           Aller a Z; }
suite_decal_lettre:
    sub     w21, w21, 1                  //        newL--;
    add     x28, x28, 1                  //        j++;
    b       decal_lettre                 //    }
fin_decal_lettre:
    strb    w21, [x19]                   //    oldL = newL
    add     x19, x19, 1                  //    i++
    b       boucle_transformation        // }
retour4:
    adr     x19, chaine
    adr     x0, fmtSortie2
    mov     x1, x19
    bl      printf
    mov     x0, 0
    RESTORE
    ret

//=========================================//
//Opération 5 : Affichage des permutations //
permutation:
    SAVE
    mov     x19, x0                      //    x19 = chaine
    mov     x27, x1                      //    x27 = j
    mov     x28, 0                       //    x28 = length = 0
    mov     x26, x27                     //    i = j
// Trouve la longeur de la chaine
taille_chaine_op5:                       //    while(chaine[length] != 0){
    ldr     x20, [x19,x28]               //
    cbz     x20, permutation1            //
    add     x28, x28, 1                  //        length++
    b       taille_chaine_op5            //    }
permutation1:

    cmp     x28, x27                     //    if (length == j){
    b.eq    affichage                    //        cout << chaine;
                                         //    }
    sub     x28, x28, 1                  //    length--;
permutation_boucle:
    cmp     x26, x28                     //    while (i <= length){
    b.hi    fin_permutation              //
                                         //
    ldrb    w21, [x19, x27]              //        swap(chaine[j], chaine[i]);
    ldrb    w22, [x19, x26]              //
    strb    w21, [x19, x26]              //
    strb    w22, [x19, x27]              //
                                         //
    add     x27, x27, 1                  //
    mov     x0, x19                      //
    mov     x1, x27                      //
    bl      permutation                  //        permutation(chaine, j+1);
    mov     x19, x0                      //
    mov     x27, x1                      //
    sub     x27, x27, 1                  //
                                         //
    ldrb    w21, [x19, x27]              //        swap(chaine[j], chaine[i]);
    ldrb    w22, [x19, x26]              //
    strb    w21, [x19, x26]              //
    strb    w22, [x19, x27]              //
    add     x26, x26, 1                  //        i++;
    b       permutation_boucle           //    }

affichage:
    adr     x0, fmtSortie2               //    printf(fmtSortie2, chaine);
    mov     x1, x19                      //
    bl      printf                       //
fin_permutation:
    mov     x0, x19                      //
    mov     x1, x27                      //
    RESTORE                              //
    ret                                  //

.section ".data"
// Mémoire allouée pour une chaîne de caractères d'au plus 1024 octets
chaine:     .skip   1024
code:       .skip   1

.section ".rodata"
// Format pour lire une chaîne de caractères d'une ligne (incluant des espaces)
fmtLecture: .asciz  "%[^\n]s"
fmtEntree:  .asciz  "%lu"
fmtSortie:  .asciz  "%lu\n"
fmtSortie2: .asciz  "%s\n"
fmtSortie3: .asciz  "%ld\n"
